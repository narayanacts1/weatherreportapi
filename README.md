# WeatherReportApi


**WeatherReportAPI Testing - Using Sernity - Cucumber -  RestAssured**

A. Description :

The weatherstack API was built to deliver accurate weather data for any locationin the world.Weatherreport API testing  is developed for testing the  retrival requests from https: //api.weatherstack.com by passing the input parameters - Mode and city.The example is reques is giben below.

Example API Request:

Run API Request -http://api.weatherstack.com/current
    ? access_key = YOUR_ACCESS_KEY
    & query = New York

Sample Request : http://api.weatherstack.com/current?access_key=37054a61778b148d526bf522e0091e91&query=New%20York

B. Test Approach  :

The entire Testing will be done using Cucumber-Sernerity-BDD ( Behavior Driven Development) frame work. Also the project will be deployed in GitLAB CI/CD pipeline.Whenever there is a new build,there will be a Artifact will be produced and the same can be downloaded in the Gitlab job results itself. The report name will be index.htmal which will be a detailed Test summary Report. It also has the feature to run and test API from report itself.

Please note that there are some scenarios which I can not able to test as the service availble for premium subscription ( Ex- Historical Weather data)

C. Test Scenerios :

The API call should be able to handle the requests from user system  and with the response time of MAXIMUM 100ms for each request.The Service will be available through an endpoint which should be active at all time for user access. The following are the high-level validations that would be carried out for the AdLoad API\

    (i) Positive Scenario
        1.Validate all the weather data attributes for the given input Mode and city ( Example - Current/City)
        2.Validate  the numeric fields value of  Json String returned using the  Mode and City name 
        3.Valid  the string  fields value of  Json String returned using the  Mode and City name 
        4.Valid  the Date   fields value of  Json String returned using the  Mode and City name 
        5.Valid  the Special Character  fields value of  Json String returned using the  Mode and City name 
        6.Valid  the json string  returned using the  Mode and City name along with the default optional  Parameters.(other optional 
        parameter  Subscription )
        7.Validate Valid  Mode,City name   Parameter with Actual City name  for Historic weather data.( Only Premium subscription )
        8.Validate the Performance ( Verify through Srenrity Report)

    (II) Negative Scenario :
        1.Validate Valid  Mode, empty value for City name   Parameter with Actual City name ,Mode  for real time weather data -RC - 601
        2.Validate InValid  Mode,valid value for City name   Parameter with Actual City name ,Mode  for real time weather data - RC -103
        3.Validate using invalid user id   Valid  Mode,valid value for City name   Parameter with Actual City name ,Mode  for real time 
        weather data  RC 105
        4.Validate using Valid  Mode,valid value for City name but no Access key Parameter with Actual City name ,Mode  for real 
        time weather data RC-101

D. Test Execution : 

    Frame Work : Cucumber BDD - Serenity 

    PreRequisite : 

    Java - JDK1.8.0
    Java - JRE1.8.0
    Cucumber(BDD) - 1.9.51
    Serenity - 2.6.0
    maven - 1.5.8
    Intellij IDEA - 2021.2.1 (IDE-Integrated development environment)
    Gitlab - Free SubscriptionE. 
    Chrome Browser - For better view of Serenity Report 
    Test API -https://any-api.com/ ( I picked Weather Report - http://api.weatherstack.com/current )
    Manual Testing : Postman ( for verify the results manually) - Optional


E. Working Folder Structure 


    src
    |-- main
    |-- test
        |-- java
                |-- com.api
                    |-- runner
                    |-- WeatherStackTestSuite.java
                    |-- StepDefinitions
                    |-- weatherStack
                        |-- fetchWeatherReport.java (Sending the request for API)
                        |-- weatherResponses.java (Getting the reponse from API)
                        |-- weatherStackStepDef.java (stepdefinitions are captured)
                |-- TestRunner.java
        |-- resources
                |-- features
                        |-- .WeatherStack(feature files)
    Target
    |-- site
        |-- serenity
        |-- Index.html ( Sernity Report)

    |-- pom.xml    
    |-- serenity.Properties ( Serenity Properties file)         


F. Pom File : 
(Important Dependencies given.for detailed refer the code stack)

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.7</maven.compiler.source>
        <maven.compiler.target>1.7</maven.compiler.target>
        <serenity.version>2.6.0</serenity.version>
        <serenity.cucumber.version>1.9.51</serenity.cucumber.version>
        <serenity.maven.version>1.5.8</serenity.maven.version>
        <junit.version>4.12</junit.version>
    <!--    <serenity.test.root></serenity.test.root>-->
        <tags></tags>
    </properties>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-core</artifactId>
            <version>${serenity.version}</version>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-screenplay</artifactId>
            <version>${serenity.version}</version>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-cucumber6</artifactId>
            <version>${serenity.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
        </dependency>
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-expressions</artifactId>
            <version>10.3.0</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-rest-assured</artifactId>
            <version>${serenity.version}</version>
        </dependency>
    

G.Runner file ( To connect Feature file WITH  Step defenition )

    WeatherStackTestSuite ==<>

    package com.api.runner;

    import io.cucumber.junit.CucumberOptions;
    import net.serenitybdd.cucumber.CucumberWithSerenity;
    import org.junit.runner.RunWith;

    @RunWith(CucumberWithSerenity.class)
    @CucumberOptions(
            plugin = {"pretty"},
            features = "src/test/resources/features",
            glue = "com.api"
    )

    public class WeatherStackTestSuite {
    }


H. Framework Set up 

        Pre requistes
        The following software needs to be installed 
        1.	JAVA JDK1.8
        2.	Maven
        3.	Gitlab
        Set up the Project
        1.	Download and installs Intellij community version from https://www.jetbrains.com/idea/download/#section=windows
        2.	Go to file  New  Project,  select Maven from the left side of the Dialogue box and choose appropriate archetype and click finish
        3.	Open the pom.xml file and add the below dependencies
        <dependencies>
                <dependency>
                    <groupId>net.serenity-bdd</groupId>
                    <artifactId>serenity-core</artifactId>
                    <version>${serenity.version}</version>
                </dependency>
                <dependency>
                    <groupId>net.serenity-bdd</groupId>
                    <artifactId>serenity-junit</artifactId>
                    <version>${serenity.version}</version>
                </dependency>
                <dependency>
                    <groupId>net.serenity-bdd</groupId>
                    <artifactId>serenity-rest-assured</artifactId>
                    <version>${serenity.version}</version>
                </dependency>
                <dependency>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-simple</artifactId>
                    <version>${slf4j.version}</version>
                </dependency>
                <dependency>
                    <groupId>junit</groupId>
                    <artifactId>junit</artifactId>
                    <version>${junit.version}</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>
        4.	Add the below plugins in the pom.xml under <build> tag
        <plugins>
                    <plugin>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <version>${maven.failsafe.plugin.version}</version>
                        <configuration>
                            <includes>
                                <include>**/*.java</include>
                            </includes>
                        </configuration>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>integration-test</goal>
                                    <goal>verify</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-compiler-plugin</artifactId>
                        <configuration>
                            <source>${java.version}</source>
                            <target>${java.version}</target>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>net.serenity-bdd.maven.plugins</groupId>
                        <artifactId>serenity-maven-plugin</artifactId>
                        <version>${serenity.maven.version}</version>
                        <dependencies>
                            <dependency>
                                <groupId>net.serenity-bdd</groupId>
                                <artifactId>serenity-core</artifactId>
                                <version>${serenity.version}</version>
                            </dependency>
                        </dependencies>
                        <executions>
                            <execution>
                                <id>serenity-reports</id>
                                <phase>post-integration-test</phase>
                                <goals>
                                    <goal>aggregate</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
        5.	Create a new future files under the future folder under SRC/Test

I. Steps Run the Test cases

    1.	Go to the project location where it is stored and Open the command prompt 
    2.	Run the below comment 
    a.	Mvn clean verify -Dtags=”<tags to run>”
    3.	Or else open the terminal In the intellij and run the above command
    


J. Steps to build YAML pipeline in Gitlab (CI/CD)

    1.	On the left page of the project default page, click on the “Settings” → “CI/CD”. Expand “Runners” section.

    2.	Configure a specific runner as per the steps given on the page. Refresh the page to see the successfully configured specific runner.

    3.	Click on the “CI/CD” → “Pipelines” on the left pane.

    4.	Click on “Create a pipeline”. This opens up a page with a default YAML.

    5.	Clear up everything and add the following:            
            a.	the container in which the pipeline should run
            image: maven:latest
            b.	variables, if needed
            variables:
            MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
            c.	Task to run the “maven test” with artifact section to upload the Serenity report.
            test:
            stage: test
            script:
                - mvn $MAVEN_OPTS clean verify "-Dtags=@WeatherReport"
            artifacts:
                paths:
        - /builds/narayanacts1/weatherreportapi/target/site/serenity/*

    6.	Commit the YML file with an appropriate comment.

    7.	This will trigger the pipeline to run automatically and within a couple of minutes the pipeline execution should complete successfully.

    8.	Click on the “Download” button on the right side of the page and a .zip file will be downloaded to your local.

    9.	Extract the contents and in the innermost folder, you can open the “index.html” file to see the report.
    
    To run a fresh instance of the pipeline:
        1.	Click on the “CI/CD” → “Pipelines” on the left pane from whatever page you are inside the project.
        2.	Find and click on the “Run pipeline” blue button.
        3.	On the next screen, select on the target branch using the dropdown. Default is the “main” branch.
        4.	Again, click on the “Run pipeline” blue button. This will trigger the pipeline to run automatically and within a couple of minutes 
                the pipeline execution should complete successfully.
        5.	Click on the “Download” button on the right side of the page and a .zip file will be downloaded to your local.
        6.	Extract the contents and in the innermost folder, you can open the “index.html” file to see the report.

K. Test Results summary : 
   The Project is successfully Executed and all the featues are working as expected.For better view kindly use Google chrome.



