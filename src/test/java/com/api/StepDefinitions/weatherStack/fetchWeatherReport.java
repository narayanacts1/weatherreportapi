package com.api.StepDefinitions.weatherStack;

import io.restassured.RestAssured;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;


public class fetchWeatherReport {
    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
    private String api_key = variables.getProperty("api.key");
    private String BASE_URI = variables.getProperty("base.uri");
    private String CURRENT_WEATHER_REPORT_BY_CITY = BASE_URI + "/{mode}";


    @Step("Validate the service is available")
    public void serviceIsAvailable(){
        Response response = RestAssured.given().get(BASE_URI);
        assertThat(response.statusCode(), isA(Integer.class));
        assertThat(response.statusCode(), is(200));
    }

    @Step("Get weather report for city {0} with {1}")
    public void pullWeatherReportByCity(String mode, String city) {
        SerenityRest.given()
                .pathParam("mode", mode)
                .queryParam("access_key", api_key)
                .queryParam("query", city)
                .get(CURRENT_WEATHER_REPORT_BY_CITY);

    }

    @Step("Get weather report for city {0} with {1} and {2}")
    public void pullWeatherReportByCityWithApiKey(String mode, String city, String access_key) {
        SerenityRest.given()
                .pathParam("mode", mode)
                .queryParam("access_key", access_key)
                .queryParam("query", city)
                .get(CURRENT_WEATHER_REPORT_BY_CITY);

    }
}
