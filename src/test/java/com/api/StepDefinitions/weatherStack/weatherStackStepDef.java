package com.api.StepDefinitions.weatherStack;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ResponseBody;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class weatherStackStepDef {
    @Steps(shared = true)
    fetchWeatherReport FetchWeatherReport;

    @Given("The Weather Stack API is available")
    public void the_weather_stack_api_is_available() {
    FetchWeatherReport.serviceIsAvailable();
    }


    @When("I send the request for {} with mode {} weather report")
    public void getWeatherReport(String City, String Mode){
        FetchWeatherReport.pullWeatherReportByCity(Mode, City);
    }

    @When("I send the request for {} with mode {} weather report with {}")
    public void getWeatherReportWithKey(String City, String Mode, String Key){
        FetchWeatherReport.pullWeatherReportByCityWithApiKey(Mode, City, Key);
    }

    @Then("I validate the response code is {int}")
    public void getWeatherReportWithKey(int status_code){
        restAssuredThat(response -> response.statusCode(status_code));
    }

    @Then("I validate city type {} and timezone {} and code {}")
    public void theResultingLocationShouldBe(String type, String timeZone, int weatherCode) {
        restAssuredThat(response -> response.body(weatherResponses.CITY_TYPE, equalTo(type)));
        restAssuredThat(response -> response.body(weatherResponses.TIMEZONE_ID, equalTo(timeZone)));
        restAssuredThat(response -> response.body(weatherResponses.WEATHER_CODE, notNullValue()));
        restAssuredThat(response -> response.body(weatherResponses.WEATHER_CODE, equalTo(weatherCode)));
    }

    @Then("I validate error code {int} and info {} and type {}")
    public void thenValidatingErrCodeAndInfo(int code, String info, String type) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_CODE, isA(Integer.class)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_CODE, equalTo(code)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_INFO, equalTo(info)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_INFO, isA(String.class)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_TYPE, isA(String.class)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_TYPE, equalTo(type)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_TYPE, containsString(type)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_MSG, isA(boolean.class)));
        restAssuredThat(response -> response.body(weatherResponses.ERROR_MSG, equalTo(weatherResponses.FALSE)));
    }


}
