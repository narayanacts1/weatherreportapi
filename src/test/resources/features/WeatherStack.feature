@WeatherReport
Feature: validating the Weather Stack for various cities

  Scenario Outline: Looking up the current Weather Report
    Given The Weather Stack API is available
    When I send the request for <City> with mode <Mode> weather report
    Then I validate the response code is <response_code>
    Then I validate city type <Type> and timezone <timeZone> and code <weather_code>
    Examples:
      |Mode     | City     | Type | timeZone            | weather_code | response_code |
      | current | New York | City | America/New_York    | 122          | 200           |
      | current | Florida  | City | America/Bogota      | 116          | 200           |
      | current | Alaska   | City | America/Mexico_City | 353          | 200           |

  Scenario Outline: Looking up the current Weather Report with negative
    Given The Weather Stack API is available
    When I send the request for <City> with mode <Mode> weather report
    Then I validate error code <error_code> and info <err_info> and type <err_type>
    Then I validate the response code is <response_code>
    Examples:
      | Mode    | City     | error_code | err_info                                                             | err_type             | response_code |
      | current | 123      | 615        | Your API request failed. Please try again or contact support.        | request_failed       | 200           |
      | current | xyz      | 615        | Your API request failed. Please try again or contact support.        | request_failed       | 200           |
      | current |          | 601        | Please specify a valid location identifier using the query parameter.| missing_query        | 200           |
      | future  | Columbia | 102        | This API Function does not exist.                                    | invalid_api_function | 200           |

  Scenario Outline: Looking up the current Weather Report with access_keys
    Given The Weather Stack API is available
    When I send the request for <City> with mode <Mode> weather report with <access_key>
    Then I validate error code <error_code> and info <err_info> and type <err_type>
    Then I validate the response code is <response_code>
    Examples:
      | Mode    | City     | access_key  | error_code | err_info                                                                                | err_type           | response_code |
      | current | New York | fda32njsea3 | 101        | You have not supplied a valid API Access Key. [Technical Support: support@apilayer.com] | invalid_access_key | 200           |
      | current | New York |             | 101        | You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]  | missing_access_key | 200           |
